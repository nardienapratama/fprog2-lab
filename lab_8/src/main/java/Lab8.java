import corporation.Company;
import employee.Staff;

import java.util.Scanner;

class Lab8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Company company = new Company("PT. TAMPAN");
        Staff.setMaxEarning(Integer.parseInt(input.nextLine()));
        while (input.hasNextLine()) {
            String[] in = input.nextLine().split(" ");
            switch (in[0]) {
                case "TAMBAH_KARYAWAN": //"ADD_EMPLOYEE":
                    company.addEmployee(in);
                    break;
                case "STATUS":
                    company.printStatus(in[1]);
                    break;
                case "TAMBAH_BAWAHAN": //"ADD_SUBORDINATE":
                    company.addSubordinate(in);
                    break;
                case "GAJIAN": //"PAYDAY":
                    company.payday();
                    break;
                case "#":
                    System.out.println("Goodbye");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid input");
                    break;
            }
        }
    }
}
