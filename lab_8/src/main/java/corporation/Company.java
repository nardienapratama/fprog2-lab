package corporation;

import employee.Employee;
import employee.Intern;
import employee.Manager;
import employee.Staff;

import java.util.ArrayList;

public class Company {
    private String name;
    private ArrayList<Employee> employees = new ArrayList<>();

    /**
     * This Object Company serve as container for all Employee
     *
     * @param name of the company
     */
    public Company(String name) {
        this.name = name;
    }

    /**
     *
     * Method for creating employee, then add the employee to company list of employee
     *
     * @param data is an Array containing the data that used for creating new employee
     *             the fromat of data is {NAME, TYPE, EARNING}
     */
    public void addEmployee(String[] data) {
        for (Employee employee : this.employees) {
            if (employee.getName().equalsIgnoreCase(data[2])) {
                System.out.printf("There's already employee named %s\n", data[2]);
                return;
            }
        }
        if (this.employees.size() < 10000) {
            switch (data[2]) {
                case "MANAGER":
                    this.employees.add(new Manager(data[1], Integer.parseInt(data[3])));
                    System.out.printf("%s started working as Manager at %s \n", data[1], this.name);
                    break;
                case "STAFF":
                    this.employees.add(new Staff(data[1], Integer.parseInt(data[3])));
                    System.out.printf("%s started working as Staff at %s \n", data[1], this.name);
                    break;
                case "INTERN":
                    this.employees.add(new Intern(data[1], Integer.parseInt(data[3])));
                    System.out.printf("%s started working as Intern at %s \n", data[1], this.name);
                    break;
                default:
                    System.out.println("Must be MANAGER or STAFF or INTERN");
                    break;
            }
        } else {
            System.out.println(String.format("%s already have 10000 employee", this.name));
        }
    }

    /**
     * This method will print the status of the employee.
     *
     * @param name the name of the employee the user want to print the status
     */
    public void printStatus(String name) {
        for (Employee employee : this.employees) {
            if (employee.getName().equalsIgnoreCase(name)) {
                System.out.println(employee);
                return;
            }
        }
        System.out.printf("There's no employee named %s\n", name);
    }

    /**
     * Method for an Employee to recruit another Employee who have lower previledge
     *
     * @param data is an Array of String containing the data, formatted {"TAMBAH_BAWAHAN", "SUPERIOR", "SUBORDINATE"}
     */
    public void addSubordinate(String[] data) {
        for (Employee superior : this.employees) {
            for (Employee subordinate : this.employees) {
                if (superior.getName().equalsIgnoreCase(data[1]) && subordinate.getName().equalsIgnoreCase(data[2])) {
                    superior.addSubordinate(subordinate);
                    return;
                }
            }
        }
        System.out.printf("Can't find %s or %s\n", data[1], data[2]);
    }

    /**
     * Method for paying every employee their monthly earning
     * This method will iterate every employee in company Array, then execute method Payday
     *
     * Special case for employee type Staff, if their monthly earning has raised over Staff's
     * max monthly earning, they will be promoted to Manager. The way i promote them is to
     * make a new manager and then replace the old Staff in Company's Array with the new Manager
     */
    public void payday() {
        int counter = 0;
        for (Employee employee : this.employees) {
            employee.payday();
            if (employee instanceof Staff && !(employee instanceof Manager)) {
                if (employee.getEarning() > Staff.getMaxEarning()) {
                    Manager newEmployee = new Manager((Staff) employee);
                    this.employees.set(counter, newEmployee);
                    System.out.printf("Congratulation, %s has been promoted to MANAGER\n", employee.getName());
                }
            }
            counter++;
        }
    }
}
