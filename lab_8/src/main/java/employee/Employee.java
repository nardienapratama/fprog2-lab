package employee;

public abstract class Employee {
    String name;
    int earning;
    int earningCount;

    /**
     * @param name of the Employee
     * @param earning is the amount of money the Employee everytime it's Payday
     */
    Employee(String name, int earning) {
        this.name = name;
        this.earning = earning;
    }

    /**
     * Method for giving the Employee their monthly earning
     */
    public abstract void payday();

    /**
     * Method for giving the Employee a 10% raise every 6th payday
     */
    protected abstract void raise();

    /**
     * Method for recruiting employee as the Emoloyee's subordinate
     *
     * @param employee is the employee that the Employee want to employ
     */
    public abstract void addSubordinate(Employee employee);

    /**
     * @return a String "EMPLOYEE_NAME EMPLOYEE_EARNING"
     */
    public abstract String toString();

    /**
     * @return the name of the Employee
     */
    public String getName() {
        return name;
    }

    /**
     * @return the monthly earning of the Employee
     */
    public int getEarning() {
        return earning;
    }

    int getEarningCount() {
        return earningCount;
    }
}
