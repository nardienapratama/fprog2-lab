package employee;

import java.util.ArrayList;

public class Staff extends Employee {
    private static int maxEarning;
    ArrayList<Employee> subordinate = new ArrayList<>();

    /**
     * @param name of the Staff
     * @param earning is the amount of the Staff's monthly earning
     */
    public Staff(String name, int earning) {
        super(name, earning);
    }

    /**
     * Method for recruiting employee as the Staff's employee. This method will check wether the
     * employee is type intern or no, because Staff can't recruit other Staff or a Manager.
     *
     * @param employee is the employee that the Employee want to employ
     */
    @Override
    public void addSubordinate(Employee employee) {
        for (Employee employed : this.subordinate) {
            if (employed.getName().equalsIgnoreCase(employee.getName())) {
                System.out.println(String.format("%s is already employed by %s", employee, this.name));
                return;
            }
        }
        if (employee instanceof Intern) {
            if (this.subordinate.size() < 10) {
                this.subordinate.add(employee);
                System.out.println(String.format("%s succcesfully employ %s", this.name, employee.getName()));
            } else {
                System.out.println(String.format("%s already have 10 subordinates", this.name));
            }
        } else {
            System.out.println("You're not previledged to have a Manager as subordinate");
        }
    }

    /**
     * Methor for giving the Staff their monthly earning
     */
    @Override
    public void payday() {
        this.earningCount++;
        if (this.earningCount % 6 == 0 && this.earning != 0) {
            raise();
        }
    }

    /**
     * Method for giving the Staff a 10% raise if this is their 6th payday
     */
    @Override
    protected void raise() {
        System.out.println(String.format("%s mengalami kenaikan gaji sebesar 10%% dari %d menjadi %d", this.name, this.earning, this.earning + this.earning / 10));
        this.earning += this.earning / 10;
    }

    /**
     * @return their status wit a format "EMPLOYEE_NAME EMPLOYEE_EARNING"
     */
    @Override
    public String toString() {
        return String.format("%s %d", this.name, this.earning);
    }

    /**
     * Methor for setting object Staff's MaxEarning
     *
     * @param maxEarning the amount of the object Staff MaxEarning
     */
    public static void setMaxEarning(int maxEarning) {
        Staff.maxEarning = maxEarning;
    }

    /**
     * @return object Staff's MaxEarning
     */
    public static int getMaxEarning() {
        return maxEarning;
    }

    /**
     * @return the Staff's current Intern
     */
    ArrayList<Employee> getSubordinate() {
        return subordinate;
    }
}
