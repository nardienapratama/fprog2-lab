package employee;

public class Manager extends Staff {
    /**
     * @param name of the Manager
     * @param earning is the amount of the Manager monthly earning
     */
    public Manager(String name, int earning) {
        super(name, earning);
    }

    /**
     * This constructor is for Staff that will be promoted, this will copy all of their previous
     * data.
     *
     * @param employee is the employee that will be promoted
     */
    public Manager(Staff employee) {
        super(employee.getName(), employee.getEarning());
        this.earningCount = employee.getEarningCount();
        this.subordinate = employee.getSubordinate();
    }

    /**
     * The difference between this and {@code addSubordinate()} in Intern is that this wont check
     * the employee's current type because Manager has the highest previledge
     *
     * @param employee is the employee that the Manager want to employ
     */
    @Override
    public void addSubordinate(Employee employee) {
        for (Employee employed : this.subordinate) {
            if (employed.getName().equalsIgnoreCase(employee.getName())) {
                System.out.println(String.format("%s is already employed by %s", employee, this.name));
                return;
            }
        }
        if (this.subordinate.size() < 10) {
            this.subordinate.add(employee);
            System.out.println(String.format("%s succcesfully employ %s", this.name, employee.getName()));
        } else {
            System.out.println(String.format("%s already have 10 subordinates", this.name));
        }
    }
}
