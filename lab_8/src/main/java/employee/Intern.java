package employee;

public class Intern extends Employee {
    /**
     * @param name of the Intern
     * @param earning is the amount of Intern's monthly earning
     */
    public Intern(String name, int earning) {
        super(name, earning);
    }

    /**
     * Method for giving the Intern their monthly earning. If this is their 6th payday, this method
     * will call {@code raise()}
     */
    @Override
    public void payday() {
        this.earningCount++;
        if (this.earningCount % 6 == 0) {
            raise();
        }
    }

    /**
     * Method for giving the Intern a 10% raise.
     */
    @Override
    protected void raise() {
        System.out.println(String.format("%s mengalami kenaikan gaji sebesar 10%% dari %d menjadi %d", this.name, this.earning, this.earning + this.earning / 10));
        this.earning += this.earning / 10;
    }

    /**
     * This method will tell the user that an Intern cannot recruit any Employee because an Intern
     * have the least previledge.
     *
     * @param employee is the employee that the Employee want to employ
     */
    @Override
    public void addSubordinate(Employee employee) {
        System.out.println("You're not previledged to have any subordinate");
    }

    /**
     * @return a String "EMPLOYEE_NAME EMPLOYEE_EARNING"
     */
    @Override
    public String toString() {
        return String.format("%s %d", this.name, this.earning);
    }
}
