package customer;
import ticket.*;
import theater.*;
public class Customer{

    private String name;
    private int age;
    private String gender;

    public Customer(String name, String gender, int age){
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    //takes the first theater (with all its properties) from the array of theaters, movie title, airtime, 3D or not
    public Ticket orderTicket(Theater theater, String title, String airtime, String threeD) {
        // make an arraylist of all movie titles?
        Ticket temp = null;
        for (int i = 0; i < theater.getMovies().length; i++) {

            if (title.equals(theater.getMovieNames().get(i))) {
                for (int j = 0; j < theater.getMovies().length; j++) {
                    if (!airtime.equals(theater.getArrayofTickets().get(j).getAirtime())) {
                        if (theater.getMovies()[j].getRating().equals("Teenagers") && this.age < 13) {
                            System.out.println(this.name + " is not old enough to watch " + title);
                            break;
                        } else if (theater.getMovies()[j].getRating().equals("Adult") && this.age < 17) {
                            System.out.println(this.name + " is not old enough to watch " + title);
                            break;
                        } else {
                            System.out.println(this.name + " has purchased tickets for " + title + " type " + threeD + " at "
                                    + theater.getCinemaName() + " on " + airtime + " for Rp." +
                                    Integer.toString(theater.getArrayofTickets().get(j).getTicketPrice()));
                            temp = theater.getArrayofTickets().get(j);
                            break;
                        }
                    } else {
                        System.out.println("Tickets for the movie " + title + " type " +
                                    threeD + " on " + airtime + " are not available in " + theater.getCinemaName());
                        break;
                    }
                }

            }


            // if (title.equals(theater.getMovies()[i].getMovieName())) { //title.equals(theater.getMovie()[i].getMovieName()
            //     if (this.age) {
            //         System.out.println(this.name + " has purchased tickets for " + this.title +
            //                 " type " + threeD.getThreeD() + " at " + theater.getCinemaName() +
            //                 " on " + this.airtime + " for Rp." + this.price);
            //     } else {
            //         System.out.println("Tickets for the movie " + this.title + " type " +
            //                 threeD.getThreeD() + " on " + this.airtime + " are not available in " +
            //                 theater.getCinemaName());
                // }

            // }
        }
        return temp;
    }

    public String findMovie(Theater theater, String title){
        String output = null;
        for(int i=0; i<theater.getMovieNames().size(); i++){
            if(title.equals(theater.getMovieNames().get(i))){
                // System.out.println("test" + theater.getMovieNames().get(i));
                theater.printInfo();    // reset the output
                break;
            }
        } 
        output = "Movie " + title + " searched by " +
                this.name + " is not in the cinema " + theater.getCinemaName();
        return output;

    }
}
