package movie;
public class Movie{
    
    private String title;
    private String genre;
    private int duration;
    private String rating;  
    private String type;    // local/import

    // Movie movie (used in Ticket.java)
    public Movie(String title, String rating, int duration, String genre, String type){
        this.title = title;
        this.rating = rating;
        this.duration = duration;
        this.genre = genre;
        this.type = type;
    }
    
    public String getMovieName(){   // get one movie name
        return this.title;
    }
    public String getRating(){
        return this.rating;
    }

    public void printInfo(){
        System.out.println("------------------------------------------------------------------");
        System.out.println("Title       : " + this.title);
        System.out.println("Genre       : " + this.genre);
        System.out.println("Duration    : " + this.duration + " minutes");
        System.out.println("Rating      : " + this.rating);
        System.out.println("Type        : " + this.type);
        System.out.println("------------------------------------------------------------------");

    }
    
    
    
    /*public Movie[] createMovieList(){
        Scanner input = new Scanner(System.in);
        Movies[] movies = input.next();  //puts the movieinput as an array in movielist
        for(int i=0; i<nummovies; i++){
           Movie[] newmovielist = new Movie(movielist[i]);
        }
        return newmovielist;*/
}