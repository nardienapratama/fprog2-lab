package theater;
import movie.*;
import ticket.*;
import java.util.ArrayList;
public class Theater{

    private String cinemaname;
    private Movie[] movies; // array of movies
    private ArrayList<Ticket> tickets;
    private int initialbalance;

    public Theater(String cinemaname, int initialbalance, ArrayList<Ticket> tickets, Movie[] movies){
        this.cinemaname = cinemaname;
        this.initialbalance = initialbalance;
        this.tickets = tickets;
        this.movies = movies;

    }

    public Movie[] getMovies(){
        return this.movies;
    }
    public ArrayList<Ticket> getArrayofTickets(){
        return this.tickets;
    }
    /*public int ticketArrayLength(){
        return tickets.size();
    }*/

    public String getCinemaName(){
        return this.cinemaname;
    }

    public int getInitialBalance(){
        return this.initialbalance;
    }

    public static String printTotalRevenueEarned(Theater[] theatersinput) {  // theatersinput - array of theaters
        int totalBalance = 0;
        for (Theater theater : theatersinput) { // for every element (i/theater) in array theatersinput
            totalBalance += theater.getInitialBalance();
        }
        System.out.println("\nKoh Mas's total money: Rp. " + totalBalance +
                "\n------------------------------------------------------------------");
        String tempinfo;
        for (int j = 0; j < theatersinput.length; j++) {
            tempinfo = "Cinema          : " + theatersinput[j].getCinemaName() +
                    "\nCash Balance    : Rp." + theatersinput[j].getInitialBalance() +"\n";
            System.out.println(tempinfo);
        }
        return "------------------------------------------------------------------";
            // theaters[0] ---> the first cinema with all its properties (name, balance)
    }

    public ArrayList<String> getMovieNames(){
        ArrayList<String> movienamelist = new ArrayList<String>();;
        for(int i=0; i<movies.length; i++){
            movienamelist.add(movies[i].getMovieName());   //[Divergent, Insurgent, etc.]
        }
        return (movienamelist); //[A, B, C]
    }


    public String changeMovieNamesIntoNonArrayLiString(){
        String temp="";
        ArrayList<String> movienamelist = new ArrayList<String>();;
        movienamelist = getMovieNames();
        for(int i=0; i<movies.length; i++){    
            if(i==(movies.length - 1)){
                temp += movienamelist.get(i);
            }
            else{
                temp += movienamelist.get(i) + ", ";
            };
                
            
        }
        return (temp); // "A, B, C"
    }
    
    // or this as the EQUIVALENT
    // public String changeMovieNamesIntoNonArrayList(){
    //     String temp="";
    //     ArrayList<String> movienamelist = new ArrayList<String>();;
    //     for(int i=0; i<movies.length; i++){
    //         movienamelist.add(movies[i].getMovieName());   //[Divergent, Insurgent, etc.]
    //         temp += movienamelist.get(i) + ", ";
    //     }
    //     return (temp);

    public void printInfo(){
        System.out.println("------------------------------------------------------------------\n" +
            "Cinema                         : " + this.cinemaname +
            "\nCash Balance                   : " + this.initialbalance +
            "\nNumber of tickets available    : " + this.tickets.size() +
            "\nMovie list available           : " + this.changeMovieNamesIntoNonArrayLiString() +
            "\n------------------------------------------------------------------");
    }
}
