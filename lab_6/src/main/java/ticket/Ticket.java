package ticket;
import movie.*;
public class Ticket{
    private Movie movie;    // variable - what's stored is the name of the movie taken from each index in 'Movie[] movies' in lab6.java
    private String airtime; //the day it airs
    private boolean threeD;
    private int price;

    public Ticket(Movie movie, String airtime, boolean threeD){
        this.movie = movie;   // has all properties of movie (title, rating, duration,etc.)
        this.airtime = airtime;
        this.threeD = threeD;
        
        if(this.airtime.equals("Saturday") || this.airtime.equals("Sunday")){
            this.price = 100000;
        }
        else{
            this.price = 60000;
        }
        if(threeD == true){
            this.price = this.price + (this.price*(20/100));
        }
    }

    public String getAirtime(){
        return this.airtime;
    }
    public int getTicketPrice(){
        return this.price;
    }

    public String getThreeD(){
        if(threeD == true){
            return "3 Dimensions";
        }
        else{
            return "Ordinary";
        }
    }

    public String retInfo(){
        return "------------------------------------------------------------------" +
                    "Movies         : " + this.movie.getMovieName() +
                    "Showtimes      : " + this.airtime +
                    "Type           : " + this.getThreeD() +
                    "------------------------------------------------------------------";
    }



}