package event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 * A class representing an event and its properties
 */
public class Event implements Comparable<Event> {
    private String name;
    private GregorianCalendar beginTime;
    private GregorianCalendar endTime;
    private BigInteger costPerHour;

    public Event(String name, GregorianCalendar beginTime, GregorianCalendar endTime, BigInteger costPerHour) {
        this.name = name;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.costPerHour = costPerHour;
    }

    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }

    // TODO: Implement toString()

    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.


    public BigInteger getCost() {
        return costPerHour;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return this.name +
                "\nWaktu mulai: " + sdf.format(this.beginTime.getTime()) +
                "\nWaktu selesai: " + sdf.format(this.endTime.getTime()) +
                "\nBiaya kehadiran: " + costPerHour.toString();
    }

    public boolean isCompatible(Event other) {
        if (beginTime.before(other.beginTime) && endTime.before(other.beginTime)) {
            return true;
        } else if (other.beginTime.before(beginTime) && other.endTime.before(beginTime)) {
            return true;
        } else
            return endTime.compareTo(other.beginTime) == 0 || other.endTime.compareTo(beginTime) == 0;
    }

    @Override
    public int compareTo(Event event) {
        return beginTime.compareTo(event.beginTime);
    }

    public Event cloneEvent() {
        return new Event(this.name, this.beginTime, this.endTime, this.costPerHour);
    }
}
