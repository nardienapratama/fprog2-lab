import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    ArrayList<Player> eatenplayers = new ArrayList<Player>();   //eaten players in game

    public ArrayList<Player> getListPlayers(){
        return this.player;
    }

    public ArrayList<Player> getEatenplayers() {
        return this.eatenplayers;
    }

    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        Player temp = null;
        if(getListPlayers().size()>0) {
            for (int i = 0; i < getListPlayers().size(); i++) {
                if (name.equals(getListPlayers().get(i).getName())) {
                    temp = getListPlayers().get(i); // returns the player with all its attributes -- 'Anna'
                }
            }
        }else {
            temp = null;
        }
        return temp;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){ //add("Almarhum", "Human", 0)
        Player temp = null;
        String var = "";
        if(find(chara) == null){    //if found
            if (tipe.equals("Human")) {
                temp = new Human(chara, tipe, hp);
            } else if(tipe.equals("Magician")){
                temp = new Magician(chara, tipe, hp);
            }else{
                temp = new Monster(chara, tipe, hp);
            }
            player.add(temp);
            var = (chara + " ditambah ke game");
        }else if(chara.equals((find(chara)).getName())){
            var = ("Sudah ada karakter bernama " + chara);
        }
        return var;
//        Player temp = null;
//        try {
//            if (tipe.equals("Human")) {
//                temp = new Human(chara, tipe, hp);
//            } else {
//                temp = new Monster(chara, tipe, hp);
//            }
//        } finally {
//            if (temp == (find(chara))) {
//                return ("Sudah ada karakter bernama " + chara);
//            } else {
//                player.add(temp);
//                System.out.println(temp);
//                System.out.println(find(chara));
//                return (chara + " ditambah ke game");
//            }
//        }

//        /*
//        for(Player var : getListPlayers()){
//            if(var.equals(temp)){
//                result = (chara + "has been added to the game");
//                break;
//            } else{
//                setListPlayers(temp);
//                result = (chara + "is added to the game");
//                break;
//            }
//        }
//        return result;*/
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        Player temp = null;
        String var = "";
        if(find(chara) == null){    //if found
            if (tipe.equals("Monster")) {
                temp = new Monster(chara, tipe, hp, roar);
                player.add(temp);
                var = (chara + " ditambah ke game");
            } else {
                var = (chara + " tidak bisa berteriak");
            }
        }else if(chara.equals((find(chara)).getName())){
            var = ("Sudah ada karakter bernama " + chara);
        }
        return var;
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        String temp = "";
        if (find(chara) != null) {  //means there is a character called chara
            for(int i=0; i<getListPlayers().size(); i++){
                if(player.get(i).equals(find(chara))){  // if player in arraylist = to chara player
                    player.remove(i);
                    temp = (chara + " dihapus dari game");
                }
            }
        } else {
            temp = ("Tidak ada " + chara);
        }
        return temp;
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        String var = "";
        if(find(chara) != null){    //character is found
            Player character = find(chara);
            var = (character.getType() + " " + character.getName() + "\n" +
                    "HP: " + character.getHp() + "\n");
            if(character.getAlive() == true) {
                var += ("Masih hidup\n");
            }else{
                var += ("Sudah meninggal dunia dengan damai\n");
            }
            if(character.getDiet().size() > 0){
                for(int i=0; i<character.getDiet().size(); i++){
                    var += ("Memakan " + character.getDiet().get(i) +"\n");
                }
            }else{
                var += ("Belum memakan siapa siapa\n");
            }
        }
        return var;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String temp = "";
        for(int i=0; i<getListPlayers().size();i++){
            Player character = getListPlayers().get(i);
            temp += status(character.getName());
        }
        return temp;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        Player character = find(chara);
        String var = "";
        for(int i=0; i<character.getDiet().size();i++){
            var += (character.getDiet().get(i).getType() + " " +character.getDiet().get(i).getName() + "\n");
        }
        return var;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String result = "";
        String finalresult = "";
        if(getEatenplayers().size() > 0){
            for(int i=0; i<getEatenplayers().size(); i++){
                if(i == getEatenplayers().size() - 1) {
                    result += getEatenplayers().get(i).getType() + " " + getEatenplayers().get(i).getName();
                }else{
                    result += getEatenplayers().get(i).getType() + " " + getEatenplayers().get(i).getName() + ", ";
                }
                finalresult = ("Has been eaten : " + result);
            }
        }else{
            finalresult = "Belum ada yang dimakan";
        }
        return finalresult;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player attacker = find(meName);
        Player attacked = find(enemyName);
        String var ="";
        if(meName.equals(attacker.getName()) && enemyName.equals(attacked.getName())){  // if both players are found
            if(attacker.canAttack()){
                if(attacker.getType().equals("Human")){
                    ((Human)attacker).attack(attacked);         // playereat eats eatenplayer
                    var = ("Nyawa " + enemyName + " " + attacked.getHp());
                }else if(attacker.getType().equals("Magician")){
                    ((Magician)attacker).attack(attacked);
                    var = ("Nyawa " + enemyName + " " + attacked.getHp());
                }else{
                    ((Monster)attacker).attack(attacked);         // playereat eats eatenplayer
                    var = ("Nyawa " + enemyName + " " + attacked.getHp());
                }
            }else{
                var = (meName + " tidak bisa attack " + enemyName);
            }
        }else{
            var = ("Tidak ada " + meName);
        }
        return var;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Player attacker = find(meName);
        Player attacked = find(enemyName);
        String var ="";
        if(meName.equals(attacker.getName()) && enemyName.equals(attacked.getName())){  // if both players are found
            if(attacker.canAttack()){
                if(attacker.getType().equals("Human")){
                    var = (meName + " tidak bisa burn " + enemyName);
                }else if(attacker.getType().equals("Magician")){
                    ((Magician)attacker).burn(attacked);
                    if(attacked.getHp() == 0){
                        var = ("Nyawa " + enemyName + " " + attacked.getHp() +
                                "\ndan matang");
                    }else {
                        var = ("Nyawa " + enemyName + " " + attacked.getHp());
                    }
                }else{
                    var = (meName + " tidak bisa burn " + enemyName);
                }
            }else{
                var = (meName + " tidak bisa burn " + enemyName);
            }
        }else{
            var = ("Tidak ada " + meName);
        }
        return var;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player playereat = find(meName);
        Player eatenplayer = find(enemyName);
        String var = "";
        if(meName.equals(playereat.getName()) && enemyName.equals(eatenplayer.getName())){  // if both players are found
            if(playereat.canEat()){
                if(playereat.getType().equals("Human")){
                    ((Human)playereat).eat(eatenplayer);         // playereat eats eatenplayer
                    eatenplayers.add(eatenplayer);                 // eatenplayer put in arraylist
                    var = (meName + " memakan " + enemyName + "\n" +
                            "Nyawa " + meName + " kini " + (playereat.getHp()));
                }else if(playereat.getType().equals("Magician")){
                    ((Magician)playereat).eat(eatenplayer);
                    eatenplayers.add(eatenplayer);
                    var = (meName + " memakan " + enemyName + "\n" +
                            "Nyawa " + meName + " kini " + (playereat.getHp()));
                }else{
                    ((Monster)playereat).eat(eatenplayer);         // playereat eats eatenplayer
                    eatenplayers.add(eatenplayer);
                    var = (meName + " memakan " + enemyName + "\n" +
                            "Nyawa " + meName + " kini " + (playereat.getHp()));
                }
            }else{
                var = (meName + " tidak bisa memakan " + enemyName);
            }
        }else{
            var = ("Tidak ada " + meName);
        }
        return var;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        String result = "";
        Player temp = find(meName);
        if(meName.equals(temp.getName())) { // if monster is found
            if(temp.getType().equals("Monster")){
                result = ((Monster)temp).getRoar();
            }else{
                result = (meName + " tidak bisa berteriak");
            }
        }else{                              // if monster isn't found
            result = "Tidak ada " + meName;
        }
        return result;
    }
}