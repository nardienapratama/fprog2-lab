package character;

//  write Monster Class here
public class Monster extends Player implements PlayerGeneral{
    String name;
    String type;
    int hp;
    String roar;

    public Monster(String name, String type, int hp){
        super(name, type, (2*hp));
        this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
    }

    public Monster(String name, String type, int hp, String roar){
        super(name, type, (2*hp));
        this.roar = roar;
    }


    public String getRoar(){
        return this.roar;
    }

    public void eat(Player playereaten){    // when monster eats something
        if(playereaten.getAlive() == false) {
            setDiet(playereaten);   //playereaten goes into the Human's diet
            setHp(15);              //the Human's diet increases
            playereaten.setEaten(true); // playereaten is now eaten
            playereaten.setAlive(false);
        }
    }

    public void attack(Player player2) {
        if(player2.getType().equals("Magician")){
            player2.setHp(2*-10);
        }else{
            player2.setHp(-10); // adds -10
        }
    }





}

