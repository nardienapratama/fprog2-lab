package character;

public class Human extends Player implements PlayerGeneral{
    String name;
    String type;
    int hp;

    public Human(String name, String type, int hp){
        super(name, type, hp);
    }

    public void eat(Player playereaten){
        if(playereaten instanceof Monster){
            if(((Monster)playereaten).getMature() == true) {
                this.setDiet(playereaten);   //playereaten goes into the Human's diet
                this.setHp(15);              //the Human's diet increases
                playereaten.setEaten(true); // eatenplayer's state is now 'eaten'
                System.out.println("000000");
                playereaten.setAlive(false);
            }
        }
    }

    public void attack(Player player2) {
        if(player2.getType().equals("Magician")){
            player2.setHp(2*-10);
        }else{
            player2.setHp(-10); // adds -10
        }
    }
}

