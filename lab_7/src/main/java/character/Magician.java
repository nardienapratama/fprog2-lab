package character;

//  write Magician Class here
public class Magician extends Player implements PlayerGeneral{
    String name;
    String type;
    int hp;

    public Magician(String name, String type, int hp){
        super(name, type, hp);
    }

    public void burn(Player burnedPlayer){
        burnedPlayer.setBurnt(true);
        try {   // dead or alive players can be burnt
            if (burnedPlayer.getType().equals("Magician")) {
                burnedPlayer.setHp(2 * (-10));  // if magician twice the damage
            } else {
                burnedPlayer.setHp(-10);    // if Hp goes below 0, it stays as 0
            }
        }finally{
            if(burnedPlayer.getAlive() == false){  //players who die from burn/already dead but burnt anyway will mature
                burnedPlayer.setMature(true);       // player has now matured, so can be eaten
            }
        }
    }

    public void attack(Player player2) {
        if(player2.getType().equals("Magician")){
            player2.setHp(2*-10);
        }else{
            player2.setHp(-10); // adds -10
        }
    }

    public void eat(Player playereaten){
        if(playereaten.getAlive() == false) {
            setDiet(playereaten);   //playereaten goes into the Human's diet
            setHp(15);              //the Human's diet increases
            playereaten.setEaten(true); // playereaten is now eaten
            playereaten.setAlive(false);
        }
    }


}


