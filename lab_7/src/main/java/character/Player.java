package character;
import java.util.ArrayList;

//  write Player Class here
public class Player {
    String name;
    int hp;
    // String diet;
    ArrayList<Player> diet = new ArrayList<Player>();
    boolean alive;
    String type;
    boolean eaten;
    boolean burnt;
    boolean mature;


    public Player(String name, String type, int hp) {
        this.name = name;
        this.hp = hp;
        this.diet = null;
        if (this.hp == 0) {
            this.alive = false;
        } else {
            this.alive = true;
        }
        this.type = type;
        this.eaten = false;
        this.burnt = false;
        this.mature = false;
    }

    public void status(Player player) {
        System.out.println(player.getType() + " " + name + "\n");
        System.out.println("HP: " + player.getHp() + "\n");
        if(player.getAlive() == true) {
            System.out.println("Masih hidup");
        }else{
            System.out.println("Sudah meninggal dunia dengan damai");
        };
        System.out.println(player.getDiet());
        player.getDiet();
    }

    public String getName() {
        return this.name;
    }

    public void setHp(int number) { // number = how much added/subtracted from original hp
        this.hp = this.hp + number;
        if ((this.hp < 0)  || (this.hp == 0 )){
            this.hp = 0;    // minimum 0 hp
            setAlive(false);
        }

    }

    public int getHp() {
        return this.hp;
    }

    public ArrayList<Player> getDiet() {
        return this.diet;
    }

    public void setDiet(Player eatenplayer) {
        diet.add(eatenplayer);  // [A, B, C]
        this.diet = diet;
    }


    public void setAlive(boolean input) {
        this.alive = input;
    }

    public boolean getAlive(){
        return this.alive;
    }

    public String getType() {
        return this.type;
    }

    public void setEaten(boolean input){
        this.eaten = input;
    }

    public boolean getEaten(){
        return this.eaten;
    }

    public void setBurnt(boolean input){
        this.burnt = input;
    }

    public boolean getBurnt(){
        return this.burnt;
    }

    public void setMature(boolean value){
        this.mature = value;
    }
    public boolean getMature(){
        return this.mature;
    }

    public boolean canEat(){    // if player is dead, they cannot eat
        if(this.alive = true){
            return true;
        }else{
            return false;
        }
    }

    public boolean canAttack(){
        if(this.alive = true){
            return true;
        }else{
            return false;
        }
    }


}
interface PlayerGeneral{
    //make all skeletons
    void eat(Player eatenplayer);
        // Player temp = Game.find(player1);
        // Player temp2 = Game.find(player2);
//        setDiet(eatenplayer);
//        setHp(15);
//        eatenplayer.setAlive(false);
//

    void attack(Player player2);
//        player2.setHp(-10); // adds -10




}