import java.util.Scanner;

public class RabbitHouse {
	
	public static void main(String[] args) {
		String [] arg = new String[2];					// new variable for input
		// Create a new Scanner object to read input from standard input
		Scanner input = new Scanner(System.in);
		arg = input.nextLine().split(" ");		// splits the two inputs into 2 separate strings
		if (arg[0].toLowerCase().equals("normal")){
			System.out.println(Integer.toString(Bunnies(1, arg[1].length()))); //arg[1] takes the index 1 from the splitted input, which is the second input
		}
			
	}

	public static int Bunnies(int RabbitAmount, int Counter){						// e.g. (1,5)
		if (Counter <= 1){
			return 1;
		}
		else{
			return RabbitAmount*Counter + Bunnies(RabbitAmount*Counter, Counter-1); // 1*5 + (5,4) --> 1*5 + 5*4 + (20,3) --> 1*5 + 5*4 + 20*3 + (60,2)   
		}																			// --> 1*5 + 5*4 + 20*3 + 60*2 + (120,1) --> 1*5 + 5*4 + 20*3 + 60*2 + 1
	}
}