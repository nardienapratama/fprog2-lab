import java.util.Scanner;

public class Manusia // Class name
{
    private String Nama;
    private int Umur;
    private int Uang;
    private double Kebahagiaan;

    /*Constructor for the 'Manusia' class */
    public Manusia(String Nama, int Umur){
        this.Nama = Nama;
        this.Umur = Umur;
        this.Uang = 50000;
        this.Kebahagiaan = 50.0;
    }

    public Manusia(String Nama, int Umur, int Uang){
        this.Nama = Nama;
        this.Umur = Umur;
        this.Uang = Uang;
        this.Kebahagiaan = 50.0;
    }

    /*Setters & Getters */
    public void setNama(String Nama){
        this.Nama = Nama;
    }
    public String getNama(){
        return Nama;
    }

    public void setUmur(int Umur){
        this.Umur = Umur;
    }

    public int getUmur(){
        return Umur;
    }

    public void setUang(int Uang){
        this.Uang = Uang;
    }

    public int getUang(){
        return Uang;
    }

    public void setKebahagiaan(double Kebahagiaan){
        this.Kebahagiaan = Kebahagiaan;
        if(this.Kebahagiaan>100){
            this.Kebahagiaan = 100;
        }
        if(this.Kebahagiaan<0.0){
            this.Kebahagiaan = 0;
        }
    }

    public double getKebahagiaan(){
        return Kebahagiaan;
    }

    /* Other method that is going to be implemented */
    public void beriUang(Manusia penerima){
        int panjangnama = penerima.getNama().length();
        int totalangka = 0;
        for(int i=0; i<panjangnama; i++){
          char letter = penerima.getNama().charAt(i);
          int number = (int) letter;
          totalangka += number;
        }
        int jumlahuang = totalangka *100;
        if(jumlahuang < this.getUang()){       // if the money they wanna give is less than the money they have
          this.setKebahagiaan(this.getKebahagiaan() + (jumlahuang/6000.0));   //the giver's happiness increases
          penerima.setKebahagiaan(penerima.getKebahagiaan()+(jumlahuang/6000.0));    //the receiver's happiness increases as well
          System.out.println(this.getNama() + " memberi uang sebanyak " + jumlahuang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
          this.setUang(this.getUang() - jumlahuang);                    /*subtracts the money we have by the money we give */
          penerima.setUang(penerima.getUang()+jumlahuang);              //adds the money of the receiver
        }
        else{
            System.out.println(this.getNama() + " ingin memberi uang kepada " + penerima + " namun tidak memiliki cukup uang :'(");
        }
    }
    public void beriUang(Manusia penerima, int jumlah){
        if (jumlah <= this.getUang()){
            this.setKebahagiaan(this.getKebahagiaan() + (jumlah/6000.0));
            penerima.setKebahagiaan(penerima.getKebahagiaan()+(jumlah/6000.0));
            System.out.println(this.getNama() + " memberi uang sebanyak " + jumlah + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
            this.setUang(this.getUang() - jumlah);  /*subtracts the money we have by the money we give */
            penerima.setUang(penerima.getUang()+jumlah);
        }
        else{
            System.out.println(this.getNama() + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
        }
    }
    public void bekerja(int durasi, int bebanKerja){
        if (this.getUmur() < 18){
            System.out.println(this.getNama() + " belum boleh bekerja karena masih di bawah umur D:");
        }
        else{
            int BebanKerjaTotal = durasi * bebanKerja;
            int Pendapatan = 0;
            if(BebanKerjaTotal <= this.getKebahagiaan()){
                this.Kebahagiaan -= BebanKerjaTotal;
                this.setKebahagiaan(this.Kebahagiaan);
                Pendapatan = BebanKerjaTotal * 10000;
                System.out.println(this.getNama() + " bekerja full time, total pendapatan : " + Pendapatan);
            }
            else{
                int DurasiBaru = (int)(Kebahagiaan/bebanKerja);
                BebanKerjaTotal = DurasiBaru * bebanKerja;
                Pendapatan = BebanKerjaTotal * 10000;
                this.setKebahagiaan(this.getKebahagiaan() - BebanKerjaTotal);
                System.out.println(this.getNama() + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + Pendapatan);
                }
            Uang = Uang + (int)Pendapatan;
            }
    }
    public void rekreasi(String namaTempat){
      int Biaya = namaTempat.length() * 10000;
      if(this.getUang() > Biaya){
        this.setUang(this.getUang()- Biaya);
        this.setKebahagiaan(this.getKebahagiaan() + namaTempat.length());
        System.out.println(this.getNama() + " berekreasi di " + namaTempat + ", " + this.getNama() + " senang :)");
      }
      else{
        System.out.println(this.getNama() + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
      }
    }

    public void sakit(String namaPenyakit){
      this.setKebahagiaan(this.getKebahagiaan() - namaPenyakit.length());
      System.out.println(this.getNama() + " terkena penyakit " + namaPenyakit + " :O");
    }

    public String toString(){
      return "Nama\t\t: " + this.getNama() + "\n" +
      "Umur\t\t: " + this.getUmur() + "\n" +
      "Uang\t\t: " + this.getUang() + "\n" +
      "Kebahagiaan\t: " + this.getKebahagiaan();
    }
}
