import java.util.Scanner;

public class Handler{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        BingoCard bingo_List;
        Number[][] arraylist = new Number[5][5];
        for(int i=0; i<5; i++){
            String row_bingo = input.nextLine();
			for(int j=0; j<5; j++){
                arraylist[i][j] = new Number((Integer.parseInt(row_bingo.split(" ")[j])),i,j);
            }
        }
        bingo_List = new BingoCard(arraylist);

        while(!bingo_List.isBingo()){
            String[] argument = input.nextLine().split(" ");

            if(argument[0].equals("MARK")){
                System.out.println(bingo_List.markNum(Integer.parseInt((argument[1]))));
            }
            if(argument[0].equals("INFO")){
                System.out.println(bingo_List.info());
            }
            if(argument[0].equals("RESTART")){
                bingo_List.restart();
            }
            bingo_List.checkBingo();
        }
        System.out.println("Bingo!");
        System.out.println(bingo_List.info());

        input.close();
    }
}

		