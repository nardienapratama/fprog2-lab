/**
 * This class contains the starter code (template) for working on lab 5.
 * <p>It is not mandatory to use this template for completing lab 5 programming
 * exercise. You are allowed to create your own <code>BingoCard</code> class.</p>
 * <p>As a side note: do not forget to create a new program class that has a
 * main method to run the program according to the specifications described in
 * the README file.</p>
 *
 * @author Nathaniel Nicholas
 * @author TODO: Write your name & NPM here!
 * Name: Nardiena Althafia Pratama
 * NPM: 1706067645
 */

public class BingoCard {

    private Number[][] numbers;		// make rows/columns
    private boolean isBingo;

    public BingoCard(Number[][] numbers){
        this.numbers = numbers;
        this.isBingo = false;
    }

    public Number[][] getNumbers(){
        return numbers;
    }

    public void setNumbers(Number[][] numbers){
        this.numbers = numbers;
    }

    public boolean isBingo(){
        return isBingo;
    }

    public void setBingo(boolean isBingo){
        this.isBingo = isBingo;
    }

    public void checkBingo(){
		if(
			(numbers[0][0].isChecked() && numbers[0][1].isChecked() && numbers[0][2].isChecked() && numbers[0][3].isChecked() && numbers[0][4].isChecked()) ||
			(numbers[1][0].isChecked() && numbers[1][1].isChecked() && numbers[1][2].isChecked() && numbers[1][3].isChecked() && numbers[1][4].isChecked()) ||
			(numbers[2][0].isChecked() && numbers[2][1].isChecked() && numbers[2][2].isChecked() && numbers[2][3].isChecked() && numbers[2][4].isChecked()) ||
			(numbers[3][0].isChecked() && numbers[3][1].isChecked() && numbers[3][2].isChecked() && numbers[3][3].isChecked() && numbers[3][4].isChecked()) ||
			(numbers[4][0].isChecked() && numbers[4][1].isChecked() && numbers[4][2].isChecked() && numbers[4][3].isChecked() && numbers[4][4].isChecked()) ||

			(numbers[0][0].isChecked() && numbers[1][0].isChecked() && numbers[2][0].isChecked() && numbers[3][0].isChecked() && numbers[4][0].isChecked()) ||
			(numbers[0][1].isChecked() && numbers[1][1].isChecked() && numbers[2][1].isChecked() && numbers[3][1].isChecked() && numbers[4][1].isChecked()) ||
			(numbers[0][2].isChecked() && numbers[1][2].isChecked() && numbers[2][2].isChecked() && numbers[3][2].isChecked() && numbers[4][2].isChecked()) ||
			(numbers[0][3].isChecked() && numbers[1][3].isChecked() && numbers[2][3].isChecked() && numbers[3][3].isChecked() && numbers[4][3].isChecked()) ||
			(numbers[0][4].isChecked() && numbers[1][4].isChecked() && numbers[2][4].isChecked() && numbers[3][4].isChecked() && numbers[4][4].isChecked()) ||
		
			(numbers[0][0].isChecked() && numbers[1][1].isChecked() && numbers[2][2].isChecked() && numbers[3][3].isChecked() && numbers[4][4].isChecked()) ||
			(numbers[0][4].isChecked() && numbers[1][3].isChecked() && numbers[2][2].isChecked() && numbers[3][1].isChecked() && numbers[4][0].isChecked())
		){
			this.setBingo(true);
		}
}
    public String markNum(int num){
        //TODO Implement
        for(int i=0; i<5; i++){
			for(int j=0; j<5; j++){
                if(this.numbers[i][j].getValue()==num){     // if the current number on the matrix is equal to the num input
                    if(this.numbers[i][j].isChecked() ==  true){
                        return num + " has been crossed";
                    }
                    else{
                        numbers[i][j].setChecked(true);
                        return "Crossed " + num; 
                    }
                }
            }
        }
        return num + " is not found on the card";
    }
  
    public String info() {
        //TODO Implement
        String tempinfo = "";
        for(int i=0; i<5; i++){
			for(int j=0; j<5; j++){
                if(numbers[i][j].isChecked() == true){
                    tempinfo += "| X ";
                }
                else if(Integer.toString(numbers[i][j].getValue()).length()==1){
                    tempinfo += "" + "| 0" + numbers[i][j].getValue() + " ";
                }
                else{
                    tempinfo += "" + "| " + numbers[i][j].getValue() + " ";
                }
            }
            tempinfo += "|\n";
        }
        return tempinfo;
    }

    public void restart(){
        //TODO Implement
		for(int i=0; i<5; i++){
			for(int j=0; j<5; j++){
				numbers[i][j].setChecked(false);
			}
        }
        System.out.println("Mulligan!");
    }
}