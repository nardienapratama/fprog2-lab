package pquiz1;

import pquiz1.enrollment.*;
import pquiz1.reader.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ArrayList;
import java.io.FileNotFoundException;

import java.util.Scanner;

/**
 * This class is the main program for programming quiz 1.
 *
 * @author Nardiena Althafia Pratama - 1706067645
 */
public class PQuiz1 {

    private static Path cwd = Paths.get("pquiz_1");
    public List<String> temporary = new ArrayList<String>();


    public static void main(String[] args) throws IOException {
        // Create a reference to the data file as instance of Path object
        //Path dataFile = cwd.resolve("data/uni-x-students.csv");

        /* TODO: Create an instance of class that implements CsvReader and
           reads the CSV data from the file referred by dataFile */
        //CsvReader reader = null;
        //protected String[][][]

        String filename = "uni-x-students.csv";
        String filedirectory = "./../../../../data";    // relative addressing
        File inputfile = new File(filedirectory + filename);
        Scanner input = new Scanner(inputfile);


        ArrayList<String> temporary = new ArrayList<String>();
        while(input.hasNextLine()){         // each line
            temporary.add(input.nextLine());    //["242214122,2014,S1,23211720,LAB AUDIT,3", "242214123,2014,S1,23211720,LAB AUDIT,3"]
        }
        String[][] temporary2 = new String[temporary.size()][];
        for(int i=0; i<temporary.size(); i++){
            temporary2[i] = temporary.get().split(","); //[["242214122", "2014", "S1", "23211720", "LAB AUDIT", "3"], ["sss","sss","Sss"]]
        }



        List<Student> students = input.getStudents();
        System.out.println(String.format("There are %d unique students in the records",
                students.size()));

        List<Student> studentsWhoTakeKimiaDasar = input.getStudentsEnrolledInCourse("Kimia Dasar");
        System.out.println(String.format("There are %d unique students that are taking Kimia Dasar",
                studentsWhoTakeKimiaDasar.size()));

        List<Student> studentsEnrolledIn2014 = input.getStudentsByEnrollYear(2014);
        System.out.println(String.format("There are %d unique students that enrolled in 2014",
                studentsEnrolledIn2014.size()));




        // Reserved for demo with TA/lecturer

        // End of reserved section
    }
    

}
