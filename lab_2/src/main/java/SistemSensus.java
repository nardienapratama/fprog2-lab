import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan and Arga Ghulam Ahmad
 *
 * This template is used for Programming Foundations 2 Tutorial 2 Term 2 2017/2018.
 * It is recommended to use this template as the base for completing Tutorial 2.
 * You are allowed to make modifications to the template as long it still matches
 * with the tutorial specification.
 *
 * How to use this template:
 * 1. Complete the code that labelled with TODO word
 * 2. Replace the area with dots with correct statement or expression so that
 * the program can be compiled and produced output according to the specification.
 *
 * Code Author (Student):
 * @author ....., Student ID ....., Class ....., GitLab Account: .....
 */

public class SistemSensus {

	public static void main(String[] args) {
		// Create a new Scanner object to read input from standard input
		Scanner input = new Scanner(System.in);

        // TODO This section corresponds to tutorial problem about "Slum Census"
		// The interface to get input from user
		System.out.print("CENSUS DATA PRINTING PROGRAM\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		int panjang = Integer.parseInt(input.nextLine());
		System.out.print("Lebar Tubuh (cm)       : ");
		int lebar = Integer.parseInt(input.nextLine());
		System.out.print("Tinggi Tubuh (cm)      : ");
		int tinggi = Integer.parseInt(input.nextLine());
		System.out.print("Berat Tubuh (kg)       : ");
		float berat = Float.parseFloat(input.nextLine());
		System.out.print("Jumlah Anggota Keluarga: ");
		int jumlah = Integer.parseInt(input.nextLine());
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = Integer.parseInt(input.nextLine());


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		int rasio = (int)((berat)/((panjang/100.0)*(lebar/100.0)*(tinggi/100.0)));

		String catatannew = " ";
		for (int a = 1; a <= jumlahCetakan; a++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.println();
			System.out.println();
			System.out.print("Pencetakan " + a + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			if (catatan.length() < 1){
				catatannew = "Tidak ada catatan tambahan.";
			}else{
				catatannew = "Note: " + catatan;
			};
			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA READY PRINTED FOR " + penerima +
					'\n' + "--------------------" +
					'\n' + nama + " - " + alamat +
					'\n' + "Born on " + tanggalLahir +
					'\n' + "Ratio Weight Per Volume = " + rasio + " kg / m ^ 3" +
					'\n' + catatannew;
			System.out.print(hasil); //print
		}


        // TODO This section corresponds to the bonus tutorial about "Apartment 
        // Recommendation"
        // TODO Calculate the family number from the available parameters
        // See the formula in the tutorial description


		input.close();
	}
}
